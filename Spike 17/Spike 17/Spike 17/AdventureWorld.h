#pragma once
#include <map>
#include <string>
#include "Room.h"

class AdventureWorld
{
private:
	std::map <std::string, Room*> _rooms;
	
public:
	AdventureWorld();
	~AdventureWorld();

	void printRoomNames();
	Room* getRoom(std::string name) {
		return _rooms[name];
	}
};

