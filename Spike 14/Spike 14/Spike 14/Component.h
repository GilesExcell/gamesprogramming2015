#pragma once
#include <string>
class Entity;

class Component
{
protected:
	Entity* _entity;
public:
	Component()
	{
	}

	~Component()
	{
	}
	Entity* getEntity() {
		return _entity;
	}

	virtual void receive(const std::string& msg) = 0;

	friend class Entity;
};

