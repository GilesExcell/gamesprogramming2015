#pragma once
#include <queue>
#include <map>
#include "Command.h"
#include "Inventory.h"

enum commandType
{
	LOOK,
	TAKE,
	PUT
};

class CommandProcessor
{
private:
	std::queue<Command*> _commands;
	std::map<std::string, commandType> _commandAliases;
public:
	CommandProcessor();
	~CommandProcessor();

	void runCommand();
	void getCommand(Inventory* self);
};

