#pragma once
#include "GameState.h"
class GameplayState :
	public GameState
{
public:
	GameplayState();
	~GameplayState();

	virtual void update(GameStateManager* manager);
	virtual void render();
};

