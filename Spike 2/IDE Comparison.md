# IDE Comparison

## Code::Blocks 13.12

### Creating and running a console application

* File->New->Project
	* Console Application
	* Complete the new console application wizard
* Build with Build->Build (Ctrl+F9)
* Run with Build->Run (Ctrl+F10)
* Build and run with Build->Build and Run (F9)

### Creating and using a breakpoint

* Toggle breakpoint for a line with Debug->Toggle Breakpoint (F5)
* Run debugger with Debug->Start/Continue (F8)

### Inspection of values during debugging
Values can be inspected with Debug->Debugging Window->Watches

## Netbeans 8.0.2

### Creating and running a console application

* File->New Project (Ctrl+Shift+N)
* C/C++ Application
* Add new files File->New File (Ctrl+N)
* Complete the on screen prompts
* Compile with Run->Build Project (F11)
* Run with Run->Run Project (F6)

### Creating and using a breakpoint

* Toggle line breakpoint with Debug->Toggle Line Breakpoint (Ctrl+F8) or by clicking on the line number to the left of the code
* Debug with Debug->Debug Project (Ctrl+F5)

### Inspection of values during debugging
Values can be inspected in the 'variables' window (by default attached to the bottom of the application)

## Microsoft Visual Studio 2013

### Creating and running a console application

* File->New->Project... (Ctrl+Shift+N)
	* Empty project template
		* Installed->Templates->Visual C++->General->Empty Project
	* Choose name and location for project
* Add new files Project->Add New Item (Ctrl+Shift+A)
	* Choose file type
* Compile with Build->Build Solution (Ctrl+Shift+B)
* Run with Debug->Start Debugging (F5) or Debug->Start without Debugging (Ctrl+F5)
* Stop debugging with Debug->Stop Debugging(Shift+F5)

### Creating and using a breakpoint

* Click on the bar on the left of the code *or* press F9 on the appropriate line of code
* Start the program with debugging

### Inspection of values during debugging
Values can be inspected from the 'Autos', 'Locals' and 'Watch' tool windows, found by default in the bottom left corner of the application

## Comparison

See comparison matrix

| IDE | Debugging | Value Inspection | Compiler | Time to start and open project (seconds) | Error Readability |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Code::Blocks | Yes | Yes - behind menu | Yes | 15 | Good |
| NetBeans | Yes | Yes | External | 10 | Okay |
| Visual Studio | Yes | Yes | Yes | 15 | Better |
NB: Visual studio and Netbeans took much longer on first start  but on subsequent starts all took about the same amount of time.

## Conclusion

I will continue to use Visual Studio 2013, as I am more familiar with it, and it is not outperformed by either of the other IDEs that I tested. 