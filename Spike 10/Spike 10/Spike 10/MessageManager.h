#pragma once
#include <map>
#include "MessageReceiver.h"

class MessageManager
{
private:
	std::map<std::string, MessageReceiver*> _receivers;
public:
	int sendMessage(Message* message);
	int subscribe(std::string address, MessageReceiver* receiver);
};

