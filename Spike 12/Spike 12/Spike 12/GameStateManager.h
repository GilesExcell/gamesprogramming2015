#pragma once

class GameState;

class GameStateManager
{
private:
	GameState* _state = (GameState*)0;
	GameState* _oldState = (GameState*)0;
public:
	GameStateManager();
	~GameStateManager();

	void changeState(GameState* newState);
	void update();
	void render();
	bool isOver() {
		return (_state == (GameState*)0 );
	}
};