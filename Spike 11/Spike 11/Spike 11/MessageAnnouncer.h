#pragma once
#include "MessageReceiver.h"
#include <vector>

class MessageAnnouncer :
	public MessageReceiver
{
private:
	std::vector<MessageReceiver*> _receivers;
public:
	MessageAnnouncer();
	~MessageAnnouncer();

	virtual int receiveMessage(Message* message);
	int subscribe(MessageReceiver* receiver);
};

