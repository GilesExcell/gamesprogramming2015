#include "MessageManager.h"
#include "ExampleMessageReceiver.h"
#include <iostream>

int main() {
	MessageManager* manager = new MessageManager();
	MessageReceiver* receiver = new ExampleMessageReceiver();

	manager->subscribe("example" , receiver);

	manager->sendMessage(new Message("example", "main", "Hello World"));
}