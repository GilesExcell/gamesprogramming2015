#include "SelectAdventureState.h"
#include <iostream>
#include <string>
#include "GameplayState.h"


SelectAdventureState::SelectAdventureState()
{
}


SelectAdventureState::~SelectAdventureState()
{
}

void SelectAdventureState::update(GameStateManager* manager) {
	std::string input;
	std::getline(std::cin, input);
}

void SelectAdventureState::render() {
	std::cout << "Any key to start adventure" << std::endl;
}