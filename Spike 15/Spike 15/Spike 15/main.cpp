#include "Inventory.h"
#include <iostream>
#include "CommandProcessor.h"

int main() {
	Inventory* backpack = new Inventory("Backpack");

	backpack->AddItem(new Item("Stick"));
	backpack->AddItem(new Item("Rock"));

	Inventory* bag = new Inventory("Bag");
	bag->AddItem(new Item("Shoe"));

	backpack->AddItem(bag);

	Inventory* self = new Inventory("The room");

	self->AddItem(backpack);
	CommandProcessor* controller = new CommandProcessor();

	for (;;) {
		controller->getCommand(self);
		controller->runCommand();
	}
}