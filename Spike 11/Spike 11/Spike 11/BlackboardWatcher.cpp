#include "BlackboardWatcher.h"
#include <iostream>

void BlackboardWatcher::checkBlackboard() {
	while (_blackboard->hasMessages())
	{
		_blackboard->giveMessage(this);
	}
}

int BlackboardWatcher::receiveMessage(Message* message) {
	std::cout << "Found " << message->contents << " on my blackboard." << std::endl;
	return 0;
}