#include "HallOfFameState.h"
#include <string>
#include <iostream>
#include "MainMenuState.h"

HallOfFameState::HallOfFameState()
{
}


HallOfFameState::~HallOfFameState()
{
}

void HallOfFameState::update(GameStateManager* manager) {
	std::string input;
	std::getline(std::cin, input);

	manager->changeState(new MainMenuState());
}

void HallOfFameState::render() {
	std::cout << "A Person (not you)" << std::endl <<
		"Somebody else (still not you)" << std::endl <<
		"Another person, who is not you" << std::endl;
}