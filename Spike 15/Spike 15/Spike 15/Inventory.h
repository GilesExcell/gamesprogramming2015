#pragma once
#include "Item.h"
#include <map>

class Inventory : public Item
{
private:
	std::map<std::string, Item*> _items;
public:
	Inventory(std::string name);
	~Inventory();

	void AddItem(Item* newItem);
	Item* TakeItem(std::string name);
	Item* GetItem(std::string name);

	virtual std::string print();
	
	friend std::ostream& operator<<(std::ostream& os, Inventory* inv);

	virtual bool isInv(){ return true; }
};

