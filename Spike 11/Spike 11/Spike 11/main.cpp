#include "MessageManager.h"
#include "ExampleMessageReceiver.h"
#include "MessageAnnouncer.h"
#include "BlackboardWatcher.h"
#include <iostream>

int main() {
	MessageManager* manager = new MessageManager();
	MessageAnnouncer* announcer = new MessageAnnouncer();

	manager->subscribe("announcer", announcer);

	ExampleMessageReceiver* receiver1 = new ExampleMessageReceiver();
	ExampleMessageReceiver* receiver2 = new ExampleMessageReceiver();
	ExampleMessageReceiver* receiver3 = new ExampleMessageReceiver();

	announcer->subscribe(receiver1);
	announcer->subscribe(receiver2);
	announcer->subscribe(receiver3);
	manager->sendMessage(new Message("announcer", "main", "Hello World"));

	BlackboardWatcher* blackboarder = new BlackboardWatcher(new MessageBlackboard());

	manager->subscribe("blackboard", blackboarder->getBlackboard());

	blackboarder->checkBlackboard();
	manager->sendMessage("blackboard", "main", "Hello");
	manager->sendMessage("blackboard", "main", "World");
	blackboarder->checkBlackboard();

}