#include "AdventureWorld.h"
#include <string>
#include <iostream>

int main() {
	AdventureWorld* world = new AdventureWorld();
	world->printRoomNames();
	std::string command;
	std::getline(std::cin, command);

	Room* location = world->getRoom(command);
	
	for (;;) {
		location->print();

		std::getline(std::cin, command);
		if (command == "Quit") {
			return 0;
		}

		location = location->move(command);
	}
}