#include "GameStateManager.h"
#include "MainMenuState.h"

int main() {
	GameStateManager* game = new GameStateManager();
	game->changeState(new MainMenuState());

	while (!game->isOver()){
		game->render();
		game->update();
	}
}