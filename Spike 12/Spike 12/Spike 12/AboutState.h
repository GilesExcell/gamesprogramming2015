#pragma once
#include "GameState.h"
class AboutState :
	public GameState
{
public:
	AboutState();
	~AboutState();

	virtual void update(GameStateManager* manager);
	virtual void render();
};

