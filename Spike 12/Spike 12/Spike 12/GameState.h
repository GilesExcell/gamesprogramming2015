#pragma once
#include "GameStateManager.h"

class GameState
{
public:
	virtual void update(GameStateManager* manager) = 0;
	virtual void render() = 0;
};

