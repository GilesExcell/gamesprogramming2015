#include "Inventory.h"

Inventory::Inventory()
{
}


Inventory::~Inventory()
{
}

void Inventory::AddItem(Item* newItem) {
	_items.insert(std::pair <std::string, Item* >(newItem->getName(), newItem));
}

Item* Inventory::TakeItem(std::string name) {
	std::multimap<std::string, Item*>::const_iterator it = _items.find(name);

	if (it != _items.end()) {
		Item* takenItem = it->second;
		_items.erase(it);
		return takenItem;
	}

	return NULL;
}

std::ostream& operator<<(std::ostream& os, const Inventory& inv) {
	if (inv._items.empty())
	{
		os << "Nothing" << std::endl;
	}
	for (std::multimap<std::string, Item*>::const_iterator it = inv._items.begin(); it != inv._items.end(); it++)
	{
		os << *it->second << std::endl;
	}
	return os;
}