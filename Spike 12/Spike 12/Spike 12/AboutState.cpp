#include "AboutState.h"
#include <iostream>
#include <string>
#include "MainMenuState.h"

AboutState::AboutState()
{
}


AboutState::~AboutState()
{
}

void AboutState::update(GameStateManager* manager) {
	std::string input;
	std::getline(std::cin, input);

	manager->changeState(new MainMenuState());
}

void AboutState::render() {
	std::cout << "Spike 12 - State Pattern" << std::endl;
}