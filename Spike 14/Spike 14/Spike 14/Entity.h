#pragma once
#include "Component.h"
#include <vector>

class Entity
{
private:
	std::vector<Component*> _components;
public:

	Entity()
	{
	}

	~Entity()
	{
	}

	void addComponent(Component* toAdd) {
		_components.push_back(toAdd);
		toAdd->_entity = this;
	}

	void removeComponent(Component* toRemove) {
		for (std::vector<Component*>::const_iterator it = _components.begin(); it != _components.end(); it++) {
			if (*it == toRemove)
				_components.erase(it);
		}
	}

	void send(std::string message) {
		for (unsigned int i = 0; i < _components.size(); i++){
			_components[i]->receive(message);
		}
	}
};