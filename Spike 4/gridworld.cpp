#include <iostream>
#include <cctype>
#include <conio.h>
#include <ctime>
using namespace std;

int playerX = 2;
int playerY = 7;
char map[9][8] = {
	{ '#', '#', '#', '#', '#', '#', '#', '#' },
	{ '#', 'G', ' ', 'D', '#', 'D', ' ', '#' },
	{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
	{ '#', '#', '#', ' ', '#', ' ', 'D', '#' },
	{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
	{ '#', ' ', '#', '#', '#', '#', ' ', '#' },
	{ '#', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
	{ '#', '#', ' ', '#', '#', '#', '#', '#' },
	{ '#', '#', '#', '#', '#', '#', '#', '#' } };
char movement;
bool running = true;
bool hasMove = false;
bool canNorth = false;
bool canEast = false;
bool canSouth = false;
bool canWest = false;
bool needRender = true;
bool hasInput = false;


void input() {
	// from what I can tell, there is no platform independant way of getting non-blocking input from the console in c++
	// So here's something that works for windows
	if (_kbhit()) {
		hasInput = true;
		movement = _getch();
		cout << movement << endl;
	}
}

void update() {
	if (hasInput) {
		needRender = true;
		switch (toupper(movement)) {
		case 'N':
			if (canNorth) {
				playerY--;
			}
			break;
		case 'E':
			if (canEast) {
				playerX++;
			}
			break;
		case 'S':
			if (canSouth) {
				playerY++;
			}
			break;
		case 'W':
			if (canWest) {
				playerX--;
			}
			break;
		case 'Q':
			running = false;
			needRender = false;
			break;
		default:
			cout << "Invalid input";
		}
	}

	hasInput = false;

	canNorth = false;
	canEast = false;
	canSouth = false;
	canWest = false;

	//North
	if (map[playerY - 1][playerX] != '#') {
		canNorth = true;
	}
	//East
	if (map[playerY][playerX + 1] != '#') {
		canEast = true;
	}
	//South
	if (map[playerY + 1][playerX] != '#') {
		canSouth = true;
	}
	//West
	if (map[playerY][playerX - 1] != '#') {
		canWest = true;
	}

	if (map[playerY][playerX] == 'D') {
		cout << "Arrrrgh... you've fallen down a pit." << endl
			<< "YOU HAVE DIED!" << endl
			<< "Thanks for playing. Maybe next time." << endl;
		running = false;
		needRender = false;
	}
	if (map[playerY][playerX] == 'G') {
		cout << "Wow - you've discovered a large chest filled with GOLD coins!" << endl
			<< "YOU WIN!" << endl
			<< "Thanks for playing. There probably won't be a next time." << endl;
		running = false;
		needRender = false;
	}


}

void render() {
	if (needRender) {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 8; j++) {
				if (playerY == i && playerX == j) {
					cout << 'P';
				}
				else {
					cout << map[i][j];
				}
			}
			cout << endl;
		}


		hasMove = false;
		cout << "You can move ";
		//North
		if (canNorth) {
			cout << "N";
			hasMove = true;
		}
		//East
		if (canEast) {
			if (hasMove) {
				cout << ",";
			}
			cout << "E";
			hasMove = true;
		}
		//South
		if (canSouth) {
			if (hasMove) {
				cout << ",";
			}
			cout << "S";
			hasMove = true;
		}
		//West
		if (canWest) {
			if (hasMove) {
				cout << ",";
			}
			cout << "W";
			hasMove = true;
		}
		cout << ":";
		needRender = false;
	}
}

int main()
{
	cout << "Welcome to GridWorld: Quantised Excitement. Fate is waiting for You!" << endl;
	cout << "Valid commands: N, S, E and W for direction. Q to quit the game." << endl;
	
	clock_t t;
	
	t = clock();
	while (running) {
		input();
		update();
		render();
	}

	t = clock() - t;
	cout << endl << "You played for " << (float)t / CLOCKS_PER_SEC << " seconds." << endl;
	cout << endl << "Press any key to quit." << endl;
	while (true) {
		if (_kbhit()) {
			return 0;
		}
	}
}