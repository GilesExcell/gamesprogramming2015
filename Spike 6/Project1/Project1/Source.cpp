#include "SDL.h"

int main(int argc, char* args[])
{
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_Window* window = SDL_CreateWindow("Spike 06", 100, 100, 600, 800, 0);

	SDL_Event e;

	while (true) {
		SDL_PollEvent(&e);
		if (e.type == SDL_QUIT)
			break;
	}

	SDL_Quit();

	return 0;
}