#include "SDL.h"
#include <iostream>

int main(int argc, char* args[])
{
	SDL_Init(SDL_INIT_EVERYTHING);

	std::cout << "Init" << std::endl;

	SDL_CreateWindow("Spike 07", 100, 100, 100, 100, 0);

	SDL_Event e;
	const Uint8* state = SDL_GetKeyboardState(NULL);
	for(;;) {
		if (SDL_PollEvent(&e)) {
			if (e.type == SDL_KEYUP) {
				if (e.key.keysym.sym >= SDLK_a && e.key.keysym.sym <= SDLK_z)
				{
					std::cout << "Key released: ";
					std::cout << (char)e.key.keysym.sym << " was released." << std::endl;
				}
			}

			if (e.type == SDL_KEYDOWN) {
				if (e.key.keysym.sym >= (int)'a' && e.key.keysym.sym <= (int)'z')
				{
					std::cout << "Key pressed: ";
					std::cout << (char)e.key.keysym.sym << " was pressed." << std::endl;
				}
			}
			
			if (state[SDL_SCANCODE_0])
				std::cout << "!";
			else
				std::cout << "-";
			if (state[SDL_SCANCODE_1])
				std::cout << "!";
			else
				std::cout << "-";
			if (state[SDL_SCANCODE_2])
				std::cout << "!";
			else
				std::cout << "-";
			if (state[SDL_SCANCODE_3])
				std::cout << "!";
			else
				std::cout << "-";
			if (state[SDL_SCANCODE_4])
				std::cout << "!";
			else
				std::cout << "-";
			if (state[SDL_SCANCODE_5])
				std::cout << "!";
			else
				std::cout << "-";
			if (state[SDL_SCANCODE_6])
				std::cout << "!";
			else
				std::cout << "-";
			if (state[SDL_SCANCODE_7])
				std::cout << "!";
			else
				std::cout << "-";
			if (state[SDL_SCANCODE_8])
				std::cout << "!";
			else
				std::cout << "-";
			if (state[SDL_SCANCODE_9])
				std::cout << "!";
			else
				std::cout << "-";

			std::cout << std::endl;

			if (e.type == SDL_QUIT) {
				std::cout << "Quit" << std::endl;
				break;
			}
		}
	}

	SDL_Quit();

	return 0;
}