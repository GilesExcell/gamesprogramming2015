#include "HealthComponent.h"
#include "DeathComponent.h"
#include "Entity.h"
#include <iostream>
#include <string>

int main() {
	Entity* dude = new Entity();
	dude->addComponent(new HealthComponent(10));
	dude->addComponent(new DeathComponent("ARG I AM DEAD"));

	std::string line;
	for (;;) {
		std::getline(std::cin, line);
		if (line == "quit")
		{
			break;
		}
		dude->send(line);
	}
}