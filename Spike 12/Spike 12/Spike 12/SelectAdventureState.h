#pragma once
#include "GameState.h"
class SelectAdventureState :
	public GameState
{
public:
	SelectAdventureState();
	~SelectAdventureState();

	virtual void update(GameStateManager* manager);
	virtual void render();
};

