#pragma once
#include <map>
#include "MessageReceiver.h"

class MessageManager
{
private:
	std::map<std::string, MessageReceiver*> _receivers;
public:
	int sendMessage(Message* message);
	int sendMessage(std::string a, std::string s, std::string c) {
		return sendMessage(new Message(a, s, c));
	}
	int subscribe(std::string address, MessageReceiver* receiver);
};

