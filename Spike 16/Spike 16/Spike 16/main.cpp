#include <iostream>
#include "Inventory.h"

using namespace std;

int main() {
	Inventory* inv = new Inventory();
	Item* sword = new Item("sword");
	Item* shield = new Item("shield");

	cout << "Inventory contains:" << endl << *inv;

	inv->AddItem(sword);
	cout << "Added " << *sword << endl;
	inv->AddItem(shield);
	cout << "Added " << *shield << endl;


	cout << "Inventory contains:" << endl << *inv;

	cout << "Taken " << *inv->TakeItem("sword") << endl;
	cout << "Taken " << *inv->TakeItem("shield") << endl;

	cout << "Inventory contains:" << endl << *inv;

	char escape;
	cin >> escape;

	return 0;
}