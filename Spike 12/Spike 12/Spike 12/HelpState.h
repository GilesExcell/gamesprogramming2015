#pragma once
#include "GameState.h"
class HelpState :
	public GameState
{
public:
	HelpState();
	~HelpState();

	virtual void update(GameStateManager* manager);
	virtual void render();
};

