#pragma once
#include "Item.h"
#include <map>

class Inventory
{
private:
	std::multimap<std::string, Item*> _items;
public:
	Inventory();
	~Inventory();

	void AddItem(Item* newItem);
	Item* TakeItem(std::string name);

	friend std::ostream& operator<<(std::ostream& os, const Inventory& inv);
};

