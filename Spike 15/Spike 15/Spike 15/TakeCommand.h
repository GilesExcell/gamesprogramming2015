#pragma once
#include "Command.h"
#include "Inventory.h"

class TakeCommand :
	public Command
{
private:
	Inventory* _source;
	std::string _object;
	Inventory* _dest;
public:
	TakeCommand(Inventory* source, std::string object, Inventory* dest) : _source(source), _object(object), _dest(dest)
	{

	}
	~TakeCommand() {

	}

	virtual void execute() {
		Item* item = _source->TakeItem(_object);
		if (item != NULL)
			_dest->AddItem(item);
	}
};