#include "NewHighscoreState.h"
#include <iostream>
#include <string>
#include "HallOfFameState.h"

NewHighscoreState::NewHighscoreState()
{
}


NewHighscoreState::~NewHighscoreState()
{
}

void NewHighscoreState::update(GameStateManager* manager) {
	std::string input;
	std::getline(std::cin, input);

	manager->changeState(new HallOfFameState);
}

void NewHighscoreState::render() {
	std::cout << "Enter your name" << std::endl;
}