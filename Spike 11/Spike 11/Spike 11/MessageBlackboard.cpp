#include "MessageBlackboard.h"


MessageBlackboard::MessageBlackboard()
{
}


MessageBlackboard::~MessageBlackboard()
{
}

int MessageBlackboard::giveMessage(MessageReceiver* receiver){
	if (_messages.empty())
	{
		return -1;
	}
	int success = receiver->receiveMessage(_messages.front());
	_messages.pop();
	return success;
}

int MessageBlackboard::receiveMessage(Message* message) {
	_messages.push(new Message(*message));
	return 0;
}