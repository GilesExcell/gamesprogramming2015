#pragma once
#include "GameState.h"
class HallOfFameState :
	public GameState
{
public:
	HallOfFameState();
	~HallOfFameState();

	virtual void update(GameStateManager* manager);
	virtual void render();
};

