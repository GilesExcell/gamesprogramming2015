#pragma once
#include "Component.h"
#include "Entity.h"
#include <sstream>
#include <iostream>

class HealthComponent :
	public Component
{
private:
	float _maxHealth;
	float _health;
public:
	HealthComponent(float maxHealth) : _health(maxHealth), _maxHealth(maxHealth)
	{
	}

	~HealthComponent()
	{
	}

	virtual void receive(const std::string& msg) {
		std::istringstream message(msg);
		std::string word;
		message >> word;
		if (word == "dmg")
		{
			message >> word;
			_health -= std::stof(word);
			if (_health <= 0)
			{
				_health = 0;
				_entity->send("die");
			}
			else if (_health > _maxHealth) {
				_health = _maxHealth;
			}
		}
		else if (word == "die") {
			_health = 0;
		}
		else if (word == "check") {
			std::cout << _health << "/" << _maxHealth << " hp" << std::endl;
		}
	}
};

