#include "HelpState.h"
#include "MainMenuState.h"
#include <iostream>
#include <string>

HelpState::HelpState()
{
}


HelpState::~HelpState()
{
}

void HelpState::update(GameStateManager* manager) {
	std::string input;
	std::getline(std::cin, input);

	manager->changeState(new MainMenuState);
}

void HelpState::render() {
	std::cout << "Commands: " << std::endl <<
		"   Are all displayed ingame" << std::endl;
}