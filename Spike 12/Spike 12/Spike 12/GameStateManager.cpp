#include "GameStateManager.h"
#include "GameState.h"


GameStateManager::GameStateManager()
{
}


GameStateManager::~GameStateManager()
{
}

void GameStateManager::changeState(GameState* newState) {
	delete _oldState;
	_oldState = _state;
	_state = newState;
}

void GameStateManager::update() {
	_state->update(this);
}

void GameStateManager::render() {
	_state->render();
}