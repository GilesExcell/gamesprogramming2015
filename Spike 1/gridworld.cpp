#include <iostream>
#include <cctype>
using namespace std;

int playerX = 2;
int playerY = 7;
char map[9][8] = {
	{ '#', '#', '#', '#', '#', '#', '#', '#' },
	{ '#', 'G', ' ', 'D', '#', 'D', ' ', '#' },
	{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
	{ '#', '#', '#', ' ', '#', ' ', 'D', '#' },
	{ '#', ' ', ' ', ' ', '#', ' ', ' ', '#' },
	{ '#', ' ', '#', '#', '#', '#', ' ', '#' },
	{ '#', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
	{ '#', '#', ' ', '#', '#', '#', '#', '#' },
	{ '#', '#', '#', '#', '#', '#', '#', '#' } };
char movement;
bool running = true;
bool hasMove = false;
bool canNorth = false;
bool canEast = false;
bool canSouth = false;
bool canWest = false;


void input() {
	hasMove = false;
	canNorth = false;
	canEast = false;
	canSouth = false;
	canWest = false;
	cout << "You can move ";
	//North
	if (map[playerY - 1][playerX] != '#') {
		cout << "N";
		canNorth = true;
		hasMove = true;
	}
	//East
	if (map[playerY][playerX + 1] != '#') {
		if (hasMove) {
			cout << ",";
		}
		cout << "E";
		canEast = true;
		hasMove = true;
	}
	//South
	if (map[playerY + 1][playerX] != '#') {
		if (hasMove) {
			cout << ",";
		}
		cout << "S";
		canSouth = true;
		hasMove = true;
	}
	//West
	if (map[playerY][playerX - 1] != '#') {
		if (hasMove) {
			cout << ",";
		}
		cout << "W";
		canWest = true;
		hasMove = true;
	}
	cout << ":";
	cin >> movement;
}

void update() {
	switch (toupper(movement)) {
	case 'N':
		if (canNorth) {
			playerY--;
		}
		break;
	case 'E':
		if (canEast) {
			playerX++;
		}
		break;
	case 'S':
		if (canSouth) {
			playerY++;
		}
		break;
	case 'W':
		if (canWest) {
			playerX--;
		}
		break;
	case 'Q':
		running = false;
		break;
	default:
		cout << "Invalid input";
	}

	if (map[playerY][playerX] == 'D') {
		cout << "Arrrrgh... you've fallen down a pit." << endl
			<< "YOU HAVE DIED!" << endl
			<< "Thanks for playing. Maybe next time." << endl;
		running = false;
	}
	if (map[playerY][playerX] == 'G') {
		cout << "Wow - you've discovered a large chest filled with GOLD coins!" << endl
			<< "YOU WIN!" << endl
			<< "Thanks for playing. There probably won't be a next time." << endl;
		running = false;
	}
}

void render() {
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 8; j++)
		{
			if (playerY == i && playerX == j)
			{
				cout << 'P';
			} else {
				cout << map[i][j];
			}
		}
		cout << endl;
	}
}

int main()
{
	cout << "Welcome to GridWorld: Quantised Excitement. Fate is waiting for You!" << endl;
	cout << "Valid commands: N, S, E and W for direction. Q to quit the game." << endl;
	
	while (running) {
		input();
		update();
		render();
	}
	return 0;
}