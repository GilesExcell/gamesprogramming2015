#pragma once
#include "MessageReceiver.h"
#include "MessageBlackboard.h"

class BlackboardWatcher :
	public MessageReceiver
{
private:
	MessageBlackboard* _blackboard;
public:
	BlackboardWatcher(MessageBlackboard* blackboard) : _blackboard(blackboard) {}
	~BlackboardWatcher() { delete _blackboard; }

	void setBlackboard(MessageBlackboard* blackboard) {
		_blackboard = blackboard;
	}

	MessageBlackboard* getBlackboard() {
		return _blackboard;
	}

	void checkBlackboard();

	virtual int receiveMessage(Message* message);
};

