#pragma once
#include "Component.h"
#include "Entity.h"
#include <sstream>
#include <iostream>

class DeathComponent :
	public Component
{
private:
	std::string _deathMessage;
	bool dead;
public:

	DeathComponent(std::string deathMessage) : _deathMessage(deathMessage), dead(false)
	{
	}

	~DeathComponent()
	{
	}

	virtual void receive(const std::string& msg) {
		std::istringstream message(msg);
		std::string word;
		message >> word;

		if (word == "die")
		{
			if (!dead) {
				std::cout << _deathMessage << std::endl;
				dead = true;
			}
		}
		else if (word == "check") {
			if (dead)
			{
				std::cout << "DEAD" << std::endl;
			}
			else {
				std::cout << "Alive" << std::endl;
			}
		}
	}
};

