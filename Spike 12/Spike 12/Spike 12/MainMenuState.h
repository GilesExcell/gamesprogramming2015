#pragma once
#include "GameState.h"
class MainMenuState :
	public GameState
{
public:
	MainMenuState();
	~MainMenuState();

	virtual void update(GameStateManager* manager);
	virtual void render();
};

