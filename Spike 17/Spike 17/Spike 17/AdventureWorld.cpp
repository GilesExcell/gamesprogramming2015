#include "AdventureWorld.h"
#include <fstream>
#include <iostream>


AdventureWorld::AdventureWorld()
{
	std::string line;
	std::ifstream world("world.txt");
	if (world.is_open())
	{
		while (std::getline(world, line) && line != "|END OF ROOMS|") {
			if (line != "") {
				_rooms[line] = new Room(line);
			}
		}
		Room* currentRoom;

		while (std::getline(world, line) && line != "|END OF CONNECTIONS|") {
			if (line != "") {
				currentRoom = _rooms[line];
				while (std::getline(world, line) && line != "|NEXT ROOM|") {
					std::string direction = line;
					std::getline(world, line);
					currentRoom->addConnection(direction, _rooms[line]);
				}
			}
		}
	}
}


AdventureWorld::~AdventureWorld()
{
	_rooms.clear();
}

void AdventureWorld::printRoomNames() {
	for (std::map<std::string, Room*>::const_iterator it = _rooms.begin(); it != _rooms.end(); it++)
	{
		std::cout << it->first << std::endl;
	}
}