#include "MainMenuState.h"
#include "HelpState.h"
#include "AboutState.h"
#include "HallOfFameState.h"
#include "SelectAdventureState.h"
#include <iostream>
#include <string>

MainMenuState::MainMenuState()
{
}


MainMenuState::~MainMenuState()
{
}

void MainMenuState::update(GameStateManager* manager) {
	std::string input;
	std::getline(std::cin, input);

	if (input == "help") {
		manager->changeState(new HelpState());
	}
	else if (input == "about") {
		manager->changeState(new AboutState());
	}
	else if (input == "hall of fame") {
		manager->changeState(new HallOfFameState());
	}
	else if (input == "adventure") {
		manager->changeState(new SelectAdventureState());
	}
	else if (input == "quit") {
		manager->changeState(NULL);
	} else {
		std::cout << "Invalid input" << std::endl;
	}
}

void MainMenuState::render() {
	std::cout << "type help for help" << std::endl <<
		"type about for an about page" << std::endl <<
		"type hall of fame to view the hall of fame" << std::endl <<
		"type adventure to start your adventure" << std::endl <<
		"type quit to quit" << std::endl;
}