#pragma once
#include "Item.h"
#include <vector>

class Inventory
{
private:
	std::vector<Item*> _items;
public:
	Inventory();
	~Inventory();

	void AddItem(Item* newItem);
	Item* TakeItem(std::string name);

	friend std::ostream& operator<<(std::ostream& os, const Inventory& inv);
};

