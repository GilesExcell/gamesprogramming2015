#include "GameplayState.h"
#include "MainMenuState.h"
#include "NewHighscoreState.h"
#include <iostream>
#include <string>

GameplayState::GameplayState()
{
}


GameplayState::~GameplayState()
{
}

void GameplayState::update(GameStateManager* manager) {
	std::string input;
	std::getline(std::cin, input);

	if (input == "quit") {
		manager->changeState(new MainMenuState());
	} else if (input == "hiscore") {
		manager->changeState(new NewHighscoreState());
	}
}

void GameplayState::render() {
	std::cout << "you can quit or hiscore, that's it." << std::endl;
}