#pragma once
#include "MessageReceiver.h"
#include <queue>

class MessageBlackboard :
	public MessageReceiver
{
private: 
	std::queue<Message*> _messages;
public:
	MessageBlackboard();
	~MessageBlackboard();

	virtual int receiveMessage(Message* message);
	int giveMessage(MessageReceiver* receiver);

	bool hasMessages() { return (!_messages.empty()); }
};