#pragma once
#include <string>
#include <iostream>

class Item
{
protected:
	std::string _name;
public:
	Item(std::string name);
	~Item();

	std::string getName(){ return _name; }
	virtual std::string print() { return _name; }
	friend std::ostream& operator<<(std::ostream& os, Item* item);
	friend bool operator==(const Item& item, const std::string& name);

	virtual bool isInv(){ return false; }
};