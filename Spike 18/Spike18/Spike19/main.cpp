#include <iostream>
#include "Inventory.h"

using namespace std;

int main() {
	Inventory* inv = new Inventory();
	Item* sword1 = new Item("sword");
	Item* sword2 = new Item("sword");

	Item* shield = new Item("shield");

	cout << "Inventory contains:" << endl << *inv;

	inv->AddItem(sword1);
	cout << "Added " << *sword1 << endl;
	inv->AddItem(sword2);
	cout << "Added " << *sword2 << endl;
	inv->AddItem(shield);
	cout << "Added " << *shield << endl;


	cout << "Inventory contains:" << endl << *inv;

	cout << "Taken " << *inv->TakeItem("sword") << endl;
	cout << "Taken " << *inv->TakeItem("shield") << endl;

	cout << "Inventory contains:" << endl << *inv;

	cout << "Taken " << *inv->TakeItem("sword") << endl;

	cout << "Inventory contains:" << endl << *inv;

	char escape;
	cin >> escape;

	return 0;
}