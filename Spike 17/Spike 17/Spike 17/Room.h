#pragma once
#include <map>
#include <string>
#include <iostream>

class Room
{
private:
	const std::string _name;
	std::map<std::string, Room*> _connections;
public:
	Room(std::string name) : _name(name) {}

	void addConnection(std::string direction, Room* destination) {
		_connections[direction] = destination;
	}

	Room* move(std::string direction) {
		if (_connections.count(direction) == 1)
		{
			return _connections[direction];
		}
		else {
			return this;
		}
	}
	void print() {
		std::cout << "You are in the " << _name << std::endl << "You can go:" << std::endl;
		for (std::map<std::string, Room*>::const_iterator it = _connections.begin(); it != _connections.end(); it++)
		{
			std::cout << it->first << std::endl;
		}
	}
};

