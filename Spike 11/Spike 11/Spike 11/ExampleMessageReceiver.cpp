#include "ExampleMessageReceiver.h"
#include <iostream>

int ExampleMessageReceiver::receiveMessage(Message* message){
	std::cout << "Address: " << message->address << std::endl
		<< "Sender: " << message->sender << std::endl
		<< "Contents: " << message->contents << std::endl;
	return 0;
}