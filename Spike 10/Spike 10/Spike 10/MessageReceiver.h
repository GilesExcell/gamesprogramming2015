#pragma once
#include <string>

struct Message
{
	const std::string address;
	const std::string sender;
	const std::string contents;
	Message(std::string a, std::string s, std::string c) : address(a), sender(s), contents(c) {}
};

class MessageReceiver
{
public:
	virtual int receiveMessage(Message* message) = 0;
};