#include "SDL.h"
#include <iostream>

int main(int argc, char *argv[]) {
	SDL_Init(SDL_INIT_EVERYTHING);
	
	SDL_Window* window = SDL_CreateWindow("Spike 08", 100, 100, 800, 600, 0);
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);

	SDL_Event e;

	bool bgActive = true;

	bool i1Active = false;
	bool i2Active = false;
	bool i3Active = false;

	SDL_Texture* background = SDL_CreateTextureFromSurface(renderer, SDL_LoadBMP("Background.bmp"));

	SDL_Texture* imagesheet = SDL_CreateTextureFromSurface(renderer, SDL_LoadBMP("Sheet.bmp"));
	SDL_Rect image1Rect;
	image1Rect.x = 0;
	image1Rect.y = 0;
	image1Rect.w = 100;
	image1Rect.h = 100;

	SDL_Rect image2Rect;
	image2Rect.x = 100;
	image2Rect.y = 0;
	image2Rect.w = 100;
	image2Rect.h = 100;

	SDL_Rect image3Rect;
	image3Rect.x = 200;
	image3Rect.y = 0;
	image3Rect.w = 100;
	image3Rect.h = 100;

	SDL_Rect image1Pos;
	image1Pos.x = 150;
	image1Pos.y = 250;
	image1Pos.w = 100;
	image1Pos.h = 100;

	SDL_Rect image2Pos;
	image2Pos.x = 350;
	image2Pos.y = 250;
	image2Pos.w = 100;
	image2Pos.h = 100;

	SDL_Rect image3Pos;
	image3Pos.x = 550;
	image3Pos.y = 250;
	image3Pos.w = 100;
	image3Pos.h = 100;

	for (;;) {
		//Input
		SDL_PollEvent(&e);

		if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_0)
			bgActive = !bgActive;

		if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_1)
			i1Active = !i1Active;

		if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_2)
			i2Active = !i2Active;

		if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_3)
			i3Active = !i3Active;

		if (e.type == SDL_QUIT)
			break;

		//Render
		SDL_RenderClear(renderer);

		if (bgActive){
			SDL_RenderCopy(renderer, background, NULL, NULL);
		}

		if (i1Active){
			SDL_RenderCopy(renderer, imagesheet, &image1Rect, &image1Pos);
		}

		if (i2Active){
			SDL_RenderCopy(renderer, imagesheet, &image2Rect, &image2Pos);
		}

		if (i3Active){
			SDL_RenderCopy(renderer, imagesheet, &image3Rect, &image3Pos);
		}

		SDL_RenderPresent(renderer);
	}

	SDL_Quit();

	return 0;
}