#include "Item.h"


Item::Item(std::string name) : _name(name)
{
}


Item::~Item()
{
}


std::ostream& operator<<(std::ostream& os, const Item& item) {
	os << item._name;
	return os;
}


bool operator==(const Item& item, const std::string& name) {
	if (item._name == name)
		return true;
	return false;
}