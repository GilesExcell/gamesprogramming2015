#include "MessageAnnouncer.h"


MessageAnnouncer::MessageAnnouncer()
{
}


MessageAnnouncer::~MessageAnnouncer()
{
}

int MessageAnnouncer::receiveMessage(Message* message){
	for (std::vector<MessageReceiver*>::const_iterator it = _receivers.begin(); it != _receivers.end(); it++)
	{
		MessageReceiver* temp = *it;
		temp->receiveMessage(message);
	}
	return 0;
}

int MessageAnnouncer::subscribe(MessageReceiver* receiver){
	_receivers.push_back(receiver);
	return 0;
}