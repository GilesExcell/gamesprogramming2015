#include "Inventory.h"

Inventory::Inventory(std::string name) : Item(name)
{
}


Inventory::~Inventory()
{
}

void Inventory::AddItem(Item* newItem) {
	if (newItem == this)
	{
		return;
	}
	if (!_items.insert(std::pair <std::string, Item* >(newItem->getName(), newItem)).second) {
		delete newItem;
	}
}

Item* Inventory::TakeItem(std::string name) {
	std::map<std::string, Item*>::const_iterator it = _items.find(name);

	if (it != _items.end()) {
		Item* takenItem = it->second;
		_items.erase(it);
		return takenItem;
	}

	return NULL;
}

Item* Inventory::GetItem(std::string name) {
	std::map<std::string, Item*>::const_iterator it = _items.find(name);

	if (it != _items.end()) {
		Item* takenItem = it->second;
		return takenItem;
	}

	return NULL;
}

std::string Inventory::print() {
	std::string	os = _name;
	os += ", containing: ";
	os += '\n';
	if (_items.empty())
	{
		os += " - Nothing";
		os += '\n';
	}
	for (std::map<std::string, Item*>::const_iterator it = _items.begin(); it != _items.end(); it++)
	{
		os += " - " + it->second->getName();
		os += '\n';
	}

	return os;
}