#pragma once
#include <string>
#include <iostream>

class Item
{
private:
	std::string _name;
public:
	Item(std::string name);
	~Item();

	friend std::ostream& operator<<(std::ostream& os, const Item& item);
	friend bool operator==(const Item& item, const std::string& name);
};