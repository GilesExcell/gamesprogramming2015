#include "SDL.h"
#include "SDL_mixer.h"

int main(int argc, char* args[])
{
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_Window* window = SDL_CreateWindow("Spike 09", 100, 100, 800, 600, 0);
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);

	SDL_Event e;

	Mix_Music* music;
	Mix_Chunk* sfx1;
	Mix_Chunk* sfx2;
	Mix_Chunk* sfx3;

	music = Mix_LoadMUS("Metronome.wav");
	sfx1 = Mix_LoadWAV("Pluck.wav");
	sfx2 = Mix_LoadWAV("Beeooow.wav");
	sfx3 = Mix_LoadWAV("Phone.wav");
	
	Mix_PlayMusic(music, -1);

	for (;;) {
		SDL_PollEvent(&e);

		if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_0)
			if (Mix_PausedMusic())
				Mix_ResumeMusic();
			else
				Mix_PauseMusic();

		if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_1)
			Mix_PlayChannel(-1, sfx1, 0);

		if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_2)
			Mix_PlayChannel(-1, sfx2, 0);

		if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_3)
			Mix_PlayChannel(-1, sfx3, 0);

		if (e.type == SDL_QUIT)
			break;
	}

	Mix_Quit();
	SDL_Quit();

	return 0;
}