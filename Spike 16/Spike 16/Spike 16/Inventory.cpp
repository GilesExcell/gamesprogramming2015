#include "Inventory.h"

Inventory::Inventory()
{
}


Inventory::~Inventory()
{
}

void Inventory::AddItem(Item* newItem) {
	_items.push_back(newItem);
}

Item* Inventory::TakeItem(std::string name) {
	for (std::vector<Item*>::const_iterator it = _items.begin(); it != _items.end(); it++)
	{
		if (**it == name)
		{
			Item* takenItem = *it;
			_items.erase(it);
			return takenItem;
		}
	}
	return NULL;
}

std::ostream& operator<<(std::ostream& os, const Inventory& inv) {
	if (inv._items.empty())
	{
		os << "Nothing" << std::endl;
	}
	for (std::vector<Item*>::const_iterator it = inv._items.begin(); it != inv._items.end(); it++)
	{
		os << **it << std::endl;
	}
	return os;
}