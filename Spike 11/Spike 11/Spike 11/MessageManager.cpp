#include "MessageManager.h"

int MessageManager::sendMessage(Message* message) {
	if (_receivers.count(message->address) > 0)
	{
		int success = _receivers[message->address]->receiveMessage(message);
		delete message;
		return success;
	}
	return -1;
}

int MessageManager::subscribe(std::string address, MessageReceiver* receiver) {
	if (_receivers.insert(std::pair<std::string, MessageReceiver*>(address, receiver)).second)
		return 0;
	if (_receivers[address] == receiver)
		return 0;
	return 1;
}
