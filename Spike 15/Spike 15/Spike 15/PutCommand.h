#pragma once
#include "Command.h"
#include "Inventory.h"

class PutCommand :
	public Command
{
private:
	Inventory* _target;
	Item* _object;
public:

	PutCommand(Inventory* target, Item* object) : _target(target), _object(object)
	{
	}

	~PutCommand()
	{
	}

	virtual void execute() {
		_target->AddItem(_object);
	}
};

