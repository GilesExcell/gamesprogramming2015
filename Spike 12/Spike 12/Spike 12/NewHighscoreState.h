#pragma once
#include "GameState.h"
class NewHighscoreState :
	public GameState
{
public:
	NewHighscoreState();
	~NewHighscoreState();

	virtual void update(GameStateManager* manager);
	virtual void render();
};

