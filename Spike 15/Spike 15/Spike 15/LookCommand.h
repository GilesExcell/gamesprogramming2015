#pragma once
#include "Command.h"
#include "Item.h"
#include <iostream>

class LookCommand : public Command
{
private:
	Item* _target;
public:

	LookCommand(Item* target) : _target(target)
	{
	}

	~LookCommand()
	{
	}

	virtual void execute() {
		if (_target != NULL)
		{
			std::cout << _target->print();
		}
	}
};

