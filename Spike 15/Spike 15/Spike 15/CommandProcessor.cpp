#include "CommandProcessor.h"
#include <iostream>
#include <sstream>
#include "LookCommand.h"
#include "TakeCommand.h"
#include "Inventory.h"

CommandProcessor::CommandProcessor()
{
	_commandAliases["look"] = LOOK;
	_commandAliases["Look"] = LOOK;
	_commandAliases["l"] = LOOK;
	_commandAliases["L"] = LOOK;

	_commandAliases["take"] = TAKE;
	_commandAliases["Take"] = TAKE;
	_commandAliases["t"] = TAKE;
	_commandAliases["T"] = TAKE;

	_commandAliases["put"] = PUT;
	_commandAliases["Put"] = PUT;
	_commandAliases["p"] = PUT;
	_commandAliases["P"] = PUT;
	_commandAliases["drop"] = PUT;
	_commandAliases["Drop"] = PUT;
	_commandAliases["d"] = PUT;
	_commandAliases["D"] = PUT;
}


CommandProcessor::~CommandProcessor()
{
}

void CommandProcessor::runCommand() {
	if (!_commands.empty()) {
		_commands.front()->execute();
		_commands.pop();
	}
}

void CommandProcessor::getCommand(Inventory* self) {
	std::string line;
	std::getline(std::cin, line);

	std::istringstream command(line);
	std::string word;

	command >> word;
	if (_commandAliases.count(word) == 1)
	{
		commandType cType = _commandAliases[word];
		switch (cType)
		{
		case LOOK:
			if (command >> word) {
				_commands.push(new LookCommand(self->GetItem(word)));
			} else {
				_commands.push(new LookCommand(self));
			}
			break;
		case TAKE:
			if (command >> word) {
				std::string object = word;
				Item* source;
				if (command >> word && word == "from" && command >> word) {
					source = self->GetItem(word);
					if (!source->isInv()) {
						source = self;
					}
				} else {
					source = self;
				}

				_commands.push(new TakeCommand((Inventory*)source, object, self));
			}
			break;
		case PUT:
			if (command >> word) {
				std::string object = word;
				Item* dest;
				if (command >> word && word == "in" && command >> word) {
					dest = self->GetItem(word);
					if (!dest->isInv()) {
						dest = self;
					}
				}
				else {
					dest = self;
				}

				_commands.push(new PutCommand((Inventory*)dest, object));
			}
			break;
		default:
			break;
		}
	}
}